const {buildSchema} = require('graphql');

const schema = buildSchema(`
  type Query {
    allUsers: [User!]!
    getUser(id: ID): User!
  }

  type User {
    id: ID
    name: String!
    age: Int!
  }
`);

module.exports = schema;
