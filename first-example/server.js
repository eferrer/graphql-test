const express = require('express');
const {graphqlHTTP} = require('express-graphql');
const schema = require('./schema/schema');
const _ = require('lodash');

const USERS = [
    {id: 1, name: 'Jose Manuel', age: 17},
    {id: 2, name: 'Pedro Ernesto', age: 29},
    {id: 3, name: 'Victoria Anem', age: 8}
];

const root = {
    allUsers: () => USERS,
    getUser: (context) => {
        return _.find(USERS, {id: +context.id});
    }
};

var app = express();
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));
app.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));
