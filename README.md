## GraphQL Test

### Testing GraphQL

#### Primer ejemplo

- Ejecuta
```
$ cd first-example
$ npm i
$ npm node server
```

- Abre en el navegador: [localhost:4000/graphql](http://localhost:4000/graphql)
- Ejecuta tu primera consulta:

![Screenshot](assets/img/first-example_01.png)

#### Segundo ejemplo

- Ejecuta
```
$ cd second-example
$ npm i
$ npm node server
```

- Abre en el navegador: [localhost:4000/graphql](http://localhost:4000/graphql)
- Ejecuta tu primera **mutation**:

![Screenshot](assets/img/second-example_01.png)
