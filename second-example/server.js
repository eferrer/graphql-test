const express = require('express');
const {graphqlHTTP} = require('express-graphql');
const schema = require('./schema/schema');
const _ = require('lodash');

const USERS = [
    {id: 0, name: 'Jose Manuel', age: 17},
    {id: 1, name: 'Pedro Ernesto', age: 29},
    {id: 2, name: 'Victoria Anem', age: 8}
];

const root = {
    allUsers: () => USERS,
    createUser: (context) => {
        const user = {id: +USERS.length, name: context.name, age: context.age };
        USERS.push(user);
        return user;
    },
};

const app = express();
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));
app.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));
