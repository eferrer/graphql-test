const {buildSchema} = require('graphql');

const schema = buildSchema(`
  type Query {
    allUsers: [User!]!
  }

  type Mutation {
    createUser(name: String!, age: Int!): User!
  }
  
  type User {
    id: ID
    name: String!
    age: Int!
  }
`);

module.exports = schema;
